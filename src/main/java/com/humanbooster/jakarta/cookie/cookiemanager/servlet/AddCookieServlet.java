package com.humanbooster.jakarta.cookie.cookiemanager.servlet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "add-cookie", value = "/add")
public class AddCookieServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        out.println("<html>" +
                "<body>" +
                "<h1>Ajouter un cookie</h1>" +
                "<a href='/cookie/list'>Voir les cookies</a>" +
                "<form method='post'>" +
                "<div><label>Nom du cookie</label>" +
                "<input name='nom_cookie' type='text'/>" +
                "</div>" +
                "<div>" +
                "<label>Valeur du cookie</label>" +
                "<input type='text' name='value_cookie'/></div>" +
                "<div><input type='submit'/></div>" +
                "</form>" +
                "</html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String cookieName = request.getParameter("nom_cookie");
        String cookieValue = request.getParameter("value_cookie");

        Cookie cookie = new Cookie(cookieName, cookieValue);
        response.addCookie(cookie);

        response.sendRedirect(request.getContextPath()+"/list");
    }
}
