package com.humanbooster.jakarta.cookie.cookiemanager.servlet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet(name = "remove_cookie", value = "/remove")
public class RemoveCookieServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String cookieToRemove = request.getParameter("cookieName");

        Cookie[] cookies = request.getCookies();

        for(Cookie cookie:cookies){
            if(cookie.getName().equals(cookieToRemove)){
                Cookie cookieNew = new Cookie((String)cookie.getName(), "");
                cookieNew.setMaxAge(0);
                response.addCookie(cookieNew);
            }
        }

        response.sendRedirect(request.getContextPath()+"/list");

    }
}
