package com.humanbooster.jakarta.cookie.cookiemanager.servlet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "list-cookie", value = "/list")
public class ListCookieServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        Cookie[] cookies = request.getCookies();

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>Liste de tous mes cookies !</h1>");
        out.println("<a href='/cookie/add'>Ajouter un cookie</a>");
        out.println("<table>" +
                "<thead>" +
                "<th>Nom du cookie</th>" +
                "<th>Valeur du cookie</th>" +
                "<th>Action</th>" +
                "</thead>" +
                "<tbody>");

        for(Cookie cookie: cookies){
            out.println("<tr>" +
                    "<td>" + cookie.getName() + "</td>"+
                    "<td>"+cookie.getValue()+"</td>"+
                    "<td><a href='/cookie/remove?cookieName="+cookie.getName()+"'>Supprimer</td></td>"+
                    "</tr>");
        }

        out.println("</tbody>");
        out.println("</body></html>");
    }
}
